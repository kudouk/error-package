package errors

import (
	"errors"
	"fmt"

	"github.com/VividCortex/mysqlerr"
	"github.com/go-sql-driver/mysql"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"

	log "github.com/sirupsen/logrus"
)

type Operation string

type ErrorStatus string

const (
	ErrorStatus_Unexpected       ErrorStatus = ""
	ErrorStatus_PermissionDenied ErrorStatus = "permission-denied"
	ErrorStatus_Unauthenticated  ErrorStatus = "unauthenticated"
	ErrorStatus_NotFound         ErrorStatus = "not-found"
	ErrorStatus_DuplicateEntry   ErrorStatus = "duplicate-entry"
	ErrorStatus_InvalidArguement ErrorStatus = "invalid-arguement"
)

var errorStatusGrpcMap = map[ErrorStatus]codes.Code{
	ErrorStatus_Unexpected:       codes.Internal,
	ErrorStatus_PermissionDenied: codes.PermissionDenied,
	ErrorStatus_Unauthenticated:  codes.Unauthenticated,
	ErrorStatus_NotFound:         codes.NotFound,
	ErrorStatus_DuplicateEntry:   codes.AlreadyExists,
	ErrorStatus_InvalidArguement: codes.InvalidArgument,
}

// loggable indicates a kudo error
type kudo interface {
	Error() string
	Stack() []Operation
	ErrorStatus() ErrorStatus
	AddOperation(o Operation)
	SetErrorStatus(s ErrorStatus)
}

// implements the basic kudo error
type kudoError struct {
	stack     []Operation
	err       error
	errStatus ErrorStatus
}

func (e *kudoError) Stack() []Operation {
	if e == nil {
		return []Operation{}
	}
	return e.stack
}

func (e *kudoError) ErrorStatus() ErrorStatus {
	if e == nil {
		return ErrorStatus_Unexpected
	}
	return e.errStatus
}

func (e *kudoError) SetErrorStatus(s ErrorStatus) {
	if e == nil {
		return
	}
	e.errStatus = s
}

func (e *kudoError) AddOperation(o Operation) {
	if e == nil {
		return
	}
	e.stack = append(e.stack, o)
}

func (e *kudoError) Error() string {
	if e == nil {
		return ""
	}
	return e.err.Error()
}

// StackTrace prints the stack trace with given error.
// If the error is not kudo, empty string is returned.
func StackTrace(err error, format string) string {
	if format == "" {
		format = "%v%v\n"
	}
	if kudoErr, ok := err.(kudo); ok {
		var out string
		for _, o := range kudoErr.Stack() {
			out = fmt.Sprintf(format, out, o)
		}
		return out
	}
	return ""
}

// Creates new kudoerror with operation and message
func New(o Operation, err error, msg string) error {

	errorStatus := ErrorStatus_Unexpected

	if sqlErr, ok := err.(*mysql.MySQLError); ok {
		switch sqlErr.Number {
		case mysqlerr.ER_DUP_ENTRY:
			errorStatus = ErrorStatus_DuplicateEntry
		case mysqlerr.ER_NO_REFERENCED_ROW_2:
			errorStatus = ErrorStatus_NotFound
		}
	}

	if errors.Is(err, gorm.ErrRecordNotFound) {
		errorStatus = ErrorStatus_NotFound
	}

	if err == nil {
		err = errors.New(msg)
	} else if msg != "" {
		err = fmt.Errorf("%v: %v", msg, err)
	}

	return &kudoError{
		stack:     []Operation{o},
		errStatus: errorStatus,
		err:       err,
	}
}

// Errorf creates a kudo error with formatted message.
// If args contains a kudo error, apply the stack of it.
// If args doesn't contain any traceable error, record the stack trace.
func Errorf(o Operation, msg string, args ...interface{}) error {
	e := fmt.Errorf(msg, args...)
	for _, arg := range args {
		if k, ok := arg.(kudo); ok {
			k.AddOperation(o)
			return k
		}
	}
	return New(o, e, "")
}

// WrapMessagef wraps an error into a kudo error with formatted message.
// If the wrapped error is kudo, only append the error message.
// If the wrapped error is not kudo, record the operation.
func WrapMessagef(err error, o Operation, msg string, args ...interface{}) error {
	formattedMsg := fmt.Sprintf(msg, args...)
	e := fmt.Errorf("%s: %v", formattedMsg, err)
	if k, ok := err.(kudo); ok {
		k.AddOperation(o)
		return k
	}
	return New(o, e, "")
}

// AddOperation adds an operation to kudo error.
// If the wrapped error is kudo, only append the operation.
// If the wrapped error is not kudo, record the operation.
func AddOperation(err error, o Operation) error {
	if err == nil {
		return nil
	}
	if k, ok := err.(kudo); ok {
		k.AddOperation(o)
		return k
	}
	return New(o, err, "")
}

// SetErrorStatus updates error status of kudo error.
// If the error is kudo, update error status.
// If the error is not kudo, return error.
func SetErrorStatus(err error, s ErrorStatus) error {
	if k, ok := err.(kudo); ok {
		k.SetErrorStatus(s)
		return k
	}
	return err
}

func ProcessError(err error, l *log.Entry) error {
	if err == nil {
		return nil
	}
	kudoError, ok := err.(kudo)
	if !ok {
		grpcError, _ := status.FromError(err)
		return grpcError.Err()
	}

	if l != nil {
		l.
			WithFields(log.Fields{
				"stack":         StackTrace(kudoError, ""),
				"error_message": err.Error(),
			}).
			Errorln(
				fmt.Errorf("%v%v", StackTrace(kudoError, "%v%v :: "), err.Error()),
			)
	}

	return status.Error(errorStatusGrpcMap[kudoError.ErrorStatus()], kudoError.Error())
}
