module bitbucket.org/kudouk/error-package

go 1.17

require (
	github.com/VividCortex/mysqlerr v0.0.0-20201215173831-4c396ae82aac
	github.com/go-sql-driver/mysql v1.6.0
	github.com/sirupsen/logrus v1.8.1
	google.golang.org/grpc v1.42.0
	gorm.io/gorm v1.23.1
)

require (
	github.com/google/go-cmp v0.5.6 // indirect
	google.golang.org/genproto v0.0.0-20211129164237-f09f9a12af12 // indirect
)
